//
//  RouteSelectionViewController.m
//  ExploreHRM
//
//  Created by Sarah Morash on 2013-12-01.
//  Copyright (c) 2013 explorehrm. All rights reserved.
//

#import "RouteSelectionViewController.h"

@interface RouteSelectionViewController ()

@end

@implementation RouteSelectionViewController

NSArray *routeList;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    routeList = [RouteManager getSharedInstance].listOfRoutes;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [routeList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RouteModel *cellModel = [[RouteManager getSharedInstance].listOfRoutes objectAtIndex:[[RouteManager getSharedInstance].listOfRoutes count]-indexPath.item-1];
    
    
    NSArray* nibFiles = [[NSBundle mainBundle] loadNibNamed:@"RouteTableCell" owner:self options:nil];
    RouteTableCell *tableCell = [nibFiles objectAtIndex:0];
    
    tableCell.routeName.text = cellModel.endLocationName;
    tableCell.routeTime.text = cellModel.startTime;
    
    return tableCell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     // Navigation logic may go here. Create and push another view controller.
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    RouteViewController *routeVC = [storyboard instantiateViewControllerWithIdentifier:@"RouteVC"];
    
    RouteModel *cellModel = [[RouteManager getSharedInstance].listOfRoutes objectAtIndex:[[RouteManager getSharedInstance].listOfRoutes count]-indexPath.item-1];
    
    routeVC.currentModel = cellModel;
    
    [self.navigationController pushViewController:routeVC animated:YES];
}

@end
