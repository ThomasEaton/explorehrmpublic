//
//  StatisticsViewController.m
//  ExploreHRM
//
//  Created by Sarah Morash on 2013-11-17.
//  Copyright (c) 2013 explorehrm. All rights reserved.
//

#import "StatisticsViewController.h"

@interface StatisticsViewController ()

@end

@implementation StatisticsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSArray *routeList = [RouteManager getSharedInstance].listOfRoutes;
    
    double distance = 0;
    double time = 0;
    int completed = 0;
    
    // Get the distance walked and the time walked from all of the complete routes.
    for (RouteModel *model in routeList) {
        if(model.isComplete) { completed++; }
        for (int i=0;i<model.currentStep;i++) {
            DirectionsObject *dir = [model.directions objectAtIndex:i];
            distance += (double)dir.distance;
            time += (double)dir.duration;
        }
    }
    
    // Get all of the information from the Routes and create working statistics to display
    self.distanceLabel.text = [NSString stringWithFormat:@"%@%@%@",self.distanceLabel.text,[NSString stringWithFormat:@"%0.2f",distance/10000],@" KM"];
    
    if(time/60/60 < 2) {
        self.durationLabel.text = [NSString stringWithFormat:@"%@%@%@",self.durationLabel.text,[NSString stringWithFormat:@"%0.2f",time/60], @" Minutes"];
    } else {
        self.durationLabel.text = [NSString stringWithFormat:@"%@%@%@",self.durationLabel.text,[NSString stringWithFormat:@"%0.2f",time/60/60], @" Hours"];
    }

    self.completedLabel.text = [NSString stringWithFormat:@"%@%d",self.completedLabel.text,completed];
    self.createdLabel.text = [NSString stringWithFormat:@"%@%d",self.createdLabel.text,[routeList count]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Sends information to Map View Controller
- (IBAction)map:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

// Sends information to Route View Controller
- (IBAction)route:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    UIViewController *routeVC = [storyboard instantiateViewControllerWithIdentifier:@"RouteTableVC"];
    [self.navigationController pushViewController:routeVC animated:YES];
}

// Sends information to Achievements View Controller
- (IBAction)achievements:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    UIViewController *achievementsVC = [storyboard instantiateViewControllerWithIdentifier:@"AchievementsVC"];
    [self.navigationController pushViewController:achievementsVC animated:YES];
}

// Sends information to Share View Controller
- (IBAction)share:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    UIViewController *shareVC = [storyboard instantiateViewControllerWithIdentifier:@"ShareVC"];
    [self.navigationController pushViewController:shareVC animated:YES];
}

@end
