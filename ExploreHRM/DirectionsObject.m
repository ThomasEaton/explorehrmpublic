//
//  DirectionsObject.m
//  ExploreHRM
//
//  Created by Thomas Eaton on 2013-11-28.
//  Copyright (c) 2013 explorehrm. All rights reserved.
//

#import "DirectionsObject.h"

@implementation DirectionsObject

// Get a dictionary of all of the data contained by this record.
- (NSDictionary *) getSaveableDictionary {
    NSMutableDictionary *saveableDictionary = [[NSMutableDictionary alloc] init];
    
    [saveableDictionary setValue:[NSString stringWithFormat:@"%f,%f",self.startLocation.latitude,self.startLocation.longitude] forKey:@"startLocation"];
    [saveableDictionary setValue:[NSString stringWithFormat:@"%f,%f",self.endLocation.latitude,self.endLocation.longitude] forKey:@"endLocation"];
    [saveableDictionary setValue:[NSString stringWithFormat:@"%d",self.distance] forKey:@"distance"];
    [saveableDictionary setValue:[NSString stringWithFormat:@"%d",self.duration] forKey:@"duration"];
    
    return saveableDictionary;
}

// Set all of the values of this DirectionsObject from the Dictionary.
- (void) loadFromDictionary:(NSDictionary *)dictionary {
    
    NSArray *coords = [[dictionary objectForKey:@"startLocation" ] componentsSeparatedByString: @","];
    CLLocationCoordinate2D start;
    start.latitude = [[coords objectAtIndex:0] doubleValue];
    start.longitude = [[coords objectAtIndex:1] doubleValue];
    
    self.startLocation = start;
    
    coords = [[dictionary objectForKey:@"endLocation" ] componentsSeparatedByString: @","];
    CLLocationCoordinate2D end;
    end.latitude = [[coords objectAtIndex:0] doubleValue];
    end.longitude = [[coords objectAtIndex:1] doubleValue];
    
    self.endLocation = end;
    
    self.distance = [[dictionary objectForKey:@"distance"] intValue];
    self.duration = [[dictionary objectForKey:@"duration"] intValue];
}

@end
