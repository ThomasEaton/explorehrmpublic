//
//  RouteManager.m
//  ExploreHRM
//
//  Created by Thomas Eaton on 2013-12-01.
//  Copyright (c) 2013 explorehrm. All rights reserved.
//

#import "RouteManager.h"

@implementation RouteManager

+(RouteManager *)getSharedInstance {
    static RouteManager* sharedInstance;
    if(!sharedInstance) {
        sharedInstance = [[RouteManager alloc] init];
    }
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.listOfRoutes = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void) saveRoutes {
    
    NSString *directoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES)objectAtIndex:0];
    NSString *filePath = [directoryPath stringByAppendingPathComponent:@"RouteData.plist"];
    
    
    // Read from document directory
    NSMutableDictionary *routesDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    if(!routesDictionary) {
        routesDictionary = [[NSMutableDictionary alloc] init];
    }
    
    NSMutableArray *routeArray = [[NSMutableArray alloc] init];
    for (RouteModel* model in _listOfRoutes) {
        [routeArray addObject:[model getSaveableDictionary]];
    }
    
    // update the dictionary
    [routesDictionary setObject:routeArray forKey:@"routes"];
    
    // write back to file
    [routesDictionary writeToFile:filePath atomically:YES];
}

- (void) loadRoutes {
    // Get the file directory
    NSString *directoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES)objectAtIndex:0];
    NSString *filePath = [directoryPath stringByAppendingPathComponent:@"RouteData.plist"];
    
    // Load the Routres from the Dictionary
    NSMutableDictionary *routesDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSMutableArray *routesObjectArray = [[NSMutableArray alloc] init];
    if(routesDictionary) {
        NSArray *routesArray = [routesDictionary objectForKey:@"routes"];
        
        for (NSDictionary *routeDictionary in routesArray) {
            RouteModel *model = [[RouteModel alloc] init];
            [model loadFromDictionary:routeDictionary];
            [routesObjectArray addObject:model];
        }
    }
    
    // Set the current route to be equal to the last route.
    _listOfRoutes = routesObjectArray;
    
    if([routesObjectArray count] != 0) {
        _currentRoute = [_listOfRoutes objectAtIndex:[_listOfRoutes count]-1];
    }
}

@end
