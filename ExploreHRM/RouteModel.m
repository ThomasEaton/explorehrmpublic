//
//  RouteModel.m
//  ExploreHRM
//
//  Created by Thomas Eaton on 2013-12-01.
//  Copyright (c) 2013 explorehrm. All rights reserved.
//

#import "RouteModel.h"

@implementation RouteModel

- (id)init
{
    self = [super init];
    if (self) {
        self.directions = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSArray*)getAnnotationsForMap {    
    NSMutableArray *annotations = [[NSMutableArray alloc] init];
    
    for(int i=self.currentStep; i<[self.directions count];i++) {
        DirectionsObject* dir = [self.directions objectAtIndex:i];
        CustomAnnotation *annotation = [[CustomAnnotation alloc] init];
        annotation.coordinate = dir.endLocation;
        annotation.stepNumber = i;
        
        [annotations addObject:annotation];
    }
    
    return annotations;
}

// Create a Dictionary of all of the individual direction objects.
- (NSDictionary *) getSaveableDictionary {
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    if(_isComplete) {
        [dictionary setValue:[NSNumber numberWithInt:1] forKey:@"complete"];
    } else {
        [dictionary setValue:[NSNumber numberWithInt:0] forKey:@"complete"];
    }
    
    [dictionary setValue:[NSNumber numberWithInt:_currentStep] forKey:@"currentStep"];
    [dictionary setValue:_endLocationName forKey:@"endLocation"];
    [dictionary setValue:_startTime forKey:@"startTime"];

    NSMutableArray *directionsArray = [[NSMutableArray alloc] init];

    for(DirectionsObject *dir in _directions) {
        [directionsArray addObject:[dir getSaveableDictionary]];
    }

    [dictionary setValue:directionsArray forKey:@"directions" ];

    return dictionary;
}


// Load all of the data and add all of the individual directions object.
- (void) loadFromDictionary:(NSDictionary*)dictionary {
    int complete = [[dictionary objectForKey:@"complete"] intValue];
    
    if(complete == 1) { _isComplete = YES; }
    if(complete == 0) { _isComplete = NO; }
    
    _currentStep = [[dictionary objectForKey:@"currentStep"] intValue];
    
    _endLocationName = [dictionary objectForKey:@"endLocation"];
    _startTime = [dictionary objectForKey:@"startTime"];
    
    NSMutableArray *directionsObjectArray = [[NSMutableArray alloc] init];
    NSArray *directionsArray = [dictionary objectForKey:@"directions"];
    
    for(NSDictionary *directionsDictionary in directionsArray) {
        DirectionsObject *dir = [[DirectionsObject alloc] init];
        
        [dir loadFromDictionary:directionsDictionary];
        
        [directionsObjectArray addObject:dir];
    }
    
    _directions = directionsObjectArray;
}

@end
